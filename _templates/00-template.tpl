{strip}

{$fb_app_id = '490219017808002'}

{$canonical_url = '//projects.voanews.com/mosul/'}
{$voa_homepage_url = 'http://www.voanews.com/'}
{$twitter_share_text = 'Twitter share line goes here'}
{$twitter_username = '@VOANews'}
{$twitter_related = ''}

{assign var="fb_action_properties" value=['object'=>$canonical_url]}

{/strip}<!doctype html>
<html>
	<head>
		<title>{$slides[0].title}: {$slides[0].title_tagline}</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">


		<link rel="canonical" href="{$canonical_url}" />
		<link type="image/x-icon" rel="icon" href="//www.voanews.com/img/voa/favicon.ico" />
		<link rel="image_src" href="{$slides[0].title_image}" />


		<!-- for Google -->
		<meta name="description" content="{$slides[0].content|strip_tags}"/>
		<meta name="keywords" content="" />
		<meta name="author" content="" />

		<!-- for Facebook -->
		<meta property="og:locale" content="en_US">
		<meta property="og:type" content="website" />
		<meta property="og:title" content="{$slides[0].title}: {$slides[0].title_tagline}" />
		<meta property="og:description" content="{$slides[0].content|strip_tags}" />
		<meta property="og:image" content="{$slides[0].title_image}" />
		<meta property="og:url" content="{$canonical_url}" />

		<!-- for Twitter -->
		<meta property="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="@voanews">
		<meta name="twitter:creator" content="@voanews">
		<meta property="twitter:title" content="{$slides[0].title}: {$slides[0].title_tagline}">
		<meta property="twitter:description" content="{$slides[0].content|strip_tags}">
		<meta property="twitter:image" content="{$slides[0].title_image}">
		<meta name="twitter:url" content="{$canonical_url}" />


		<meta name="DISPLAYDATE" content="April 28, 2017" />
		<meta itemprop="dateModified" content="2017-04-28" />
		<meta itemprop="datePublished" content="2017-04-28" />


		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<script type="text/javascript" src="//www.voanews.com/MediaAssets2/projects/voa_graphics/resources/jquery.smartmenus.min.js"></script>
		<script type="text/javascript" src="//d3js.org/d3.v3.min.js"></script>
		<script type="text/javascript" src='//www.voanews.com/MediaAssets2/projects/voa_graphics/resources/tabletop.js'></script>
		<script type="text/javascript" src='//www.voanews.com/MediaAssets2/projects/voa_graphics/resources/leaflet/js/leaflet.js'></script>
		<link rel="stylesheet" type="text/css" href="//www.voanews.com/MediaAssets2/projects/voa_graphics/resources/leaflet/css/leaflet.css">




		<link href="//fonts.googleapis.com/css?family=Oswald" rel="stylesheet" />


		<script src="{*$canonical_url*}js/scripts.js?ts={$smarty.now}"></script>
		<link rel="stylesheet" type="text/css" href="{*$canonical_url*}css/all.css?ts={$smarty.now}">


		<script type="text/javascript">

			$(document).ready(function(){
				$("#closeWarning").click(function(){
					console.log("close the warning box");
					$(".voa__publish-warning").toggle();
				})
			})

		</script>

	</head>
<body>
{include file="metrics.tpl"}

<!--
<script type="text/javascript">
(function(a,b,c,d){
a='//tags.tiqcdn.com/utag/bbg/voa-nonpangea/prod/utag.js';
b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
})();
</script>
-->


	<div class="voa__project english">

		<div class="voa__navbar dark">
			<div class="voa__grid__full">
				<div class="voa__grid">
					<a href="http://www.voanews.com" class="voa__navbar__link" title="Return to the VOA News home page"><div class="voa__navbar__logo"></div></a><a href="#" class="voa__navbar__title">{$slides[0].title_label_nav}</a>

					<div id="menuButton" class="voa__navbar__toggle"></div>

					<nav id="main-nav" role="navigation" class="main-menu-nav">
						<ul id="main-menu" class="sm sm-clean" data-smartmenus-id="14762810590939192">
							<li id="navSection1"><a href="#section1">Section</a></li>
							<li id="navSection2"><a href="#section2">Section</a></li>
							<li><a href="javascript:void(0);" class="has-submenu" id="sm-14762810590939192-1" aria-haspopup="true" aria-controls="sm-14762810590939192-2" aria-expanded="false"><span class="sub-arrow">+</span>Submenus</a>
								<ul id="sm-14762810590939192-2" role="group" aria-hidden="true" aria-labelledby="sm-14762810590939192-1" aria-expanded="false">
									<li><a href="#sub4a">Sub A</a></li>
									<li><a href="#sub4a">Sub B</a></li>
									<li><a href="#sub4a">Sub C</a></li>
								</ul>
							</li>

							<li><a href="javascript:void(0);" class="has-submenu voa__navbar__language" id="" aria-haspopup="true" aria-controls="" aria-expanded="false"><div class="translation-icon"></div><span class="sub-arrow">+</span><span class="voa__no-tablet social-share-text ">Translations</span></a>
								<ul id="" role="group" aria-hidden="true" aria-labelledby="" aria-expanded="false">
									<li><a href="#sub4a">Language A</a></li>
									<li><a href="#sub4a">Language B</a></li>
									<li><a href="#sub4a">Language C</a></li>
								</ul>
							</li>

							<li><a id="shareTwitter" title="Share on Twitter" class="social-share-link twitter" href="javascript:void(0);"><div class="social-icon"></div><span class="social-share-text">Share on Twitter</span></a></li>
							<li><a id="shareFacebook" title="Share on Facebook" class="social-share-link facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fprojects.voanews.com%2F"><div class="social-icon"></div><span class="social-share-text">Share on Facebook</span></a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>

		<!--
		<section class="voa__section__full-width voa__section__title" style="background-image: url('{$slides[0].title_image}'); ">
			<div class="voa__title-card__shadow"></div>

			<div class="voa__grid__full">
				<div class="voa__grid">
					<div class="voa__title-card">
						<h1 class="voa__kicker">Kicker</h1>
						<h2 class="voa__title">{$slides[0].title}</h2>
					</div>
				</div>
			</div>
		</section>
		-->




		<section id="titleSlide" class="voa__section__full-width voa__section__title landing-page fullscreen" style="background-image: url('{$slides[0].title_image}'); background-position: center top 40%;">
			<div class="voa__title-card__shadow" style="z-index: 9994"></div>

			<div class="voa__grid__full">
					<div class="voa__title-label landing-page">
						<h3 class="voa__kicker">VOA Special report</h3>
					</div>
					<div class="voa__title-card landing-page">
						<div class="voa__grid__full">
							<div class="voa__grid">
								<h1 class="voa__title white" style="">{$slides[0].title}</h1>
								<h3 class="voa__deck" style="text-transform: none; color: #000">{$slides[0].title_tagline}</h3>
							</div>
						</div>
					</div>
					<div style="position: absolute; bottom: 0; width: 100%; z-index: 9999; max-width: 1280px;" >
						<a href="#intro" id="introLink">
							<div class="voa__nav__down-arrow"><span class="arrow"></span></div>
						</a>
					</div>

			</div>
		</section>




		<section id="intro" class="voa__section">
			<div class="voa__grid">
				<div class="voa__project-title" style="margin: 40px 0;">
					<h3 class="voa__graphic__readin">{$slides[0].content|strip_tags}</h3>
				</div>
			</div>
		</section>

		<div class="voa__publish-warning">
			<h3 id="closeWarning" class="voa__publish-warning__close-button">[X]</h3>
			<h3>WORK IN PROGRESS (english)</h3>
			<p>This project is still under development. Please do not share outside of the project team.</p>
		</div>


		<section class="voa__section" style="margin-bottom: 50px;">
			<div class="voa__grid">
				<div id="map" class="map__container"></div>
			</div>
		</section>



		<section class="voa__section__full-width voa__footer">
			<div class="voa__grid__full">
				<!--
				<div class="voa__grid">
					<h3 class="voa__kicker">Recent headlines</h3>
				</div>
				-->

				<div class="voa__grid">
					<div class="voa__grid__one-third voa__rule wide-right">

						<h3>Recent headlines</h3>

						{include 
							note="feed-relay makes sure an RSS feed doesn't get hit more than once per 5 minutes"
							file="_templates/00-rss.tpl" 
							url="http://projects.voanews.com/feed-relay/?url=http://www.voanews.com/api/zr\$opeuvim"
						}
					</div>

					<!--<script type="text/javascript" src="http://tools.voanews2.com/fidget/view.php?id=224&js"></script>-->



					<div class="voa__grid__one-third voa__rule wide-right">
						<h3>Project credits</h3>
								
						<p>Reported by <a href="https://twitter.com/abdulazizosman">Abdulaziz Osman</a> and <a href="https://twitter.com/PinaultNicolas">Nicolas Pinault.</a></p>
						<p>Edited by Peter Cobus.</p>
						<p>Web design and development by <a href="https://twitter.com/smekosh" target="_blank">Stephen Mekosh</a> and <a href="https://twitter.com/pp19dd" target="_blank">Dino Beslagic</a>.</p>
						<p>Project management by <a title="Steven Ferri" href="https://twitter.com/ferrimedia">Steven Ferri</a>.</p>
						<p>Content production by <a href="https://twitter.com/teff_times_two">Teffera G. Teffera</a> and Ezra Fessahaye.</p>

					</div><!-- .voa__credits -->

					<div class="voa__grid__one-third">

						<h3>About the author</h3>
						<img src="http://placehold.it/120x160" class="voa__mugshot" />
						<p><a href="https://twitter.com/stevebaragona">Steve Baragona</a> is an award-winning multimedia journalist covering science, environment and health.</p>
						<p>He spent eight years in molecular biology and infectious disease research before deciding that writing about science was more fun than doing it. He graduated from the University of North Carolina at Chapel Hill with a master’s degree in journalism in 2002.</p>
					</div><!-- .voa__credits -->
				</div>
			</div>
		</section><!-- .voa__footer -->


	</div><!-- .voa__project -->
<script type="text/javascript">
	//var public_spreadsheet_url = 'https://docs.google.com/spreadsheets/d/1ONYDSbWz57kIRUEoe-rIsQjq69JelwayZdZ_eaZW2ZA/pubhtml';//This can also be defined in the JS.
	var mode = "editing";//"editing"; //"production";
	var dictionary = {};
</script>

	

</body>
</html>