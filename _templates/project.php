<?php

//Update the class name (with underscores between words)
//Update the public $name and $folder 

class timeline_renderer_mosul extends timeline_renderer {
    public $name = 'Mosul project';
    public $folder = "renderers/mosul/";
    
    // 2016-06-19: new feature addition
    public $chronological = false;
    
    function declare_options() {
        
        // gah, superfix for superincludes bla
        $this->xfolder = "renderers/unexpected-americans/";

        $this->folder = realpath($this->folder);

        $this->options[] = 
            new timeline_renderer_option( array(
                "label" => "folder/template.tpl",
                "name" => "template"
            ));
            /*
        $this->options[] = 
            new timeline_renderer_option( array(
                "label" => "RSS URL",
                "name" => "rss_url"
            ));
            */
            /*
        $this->options[] = 
            new timeline_renderer_option( array(
                "label" => "Language (JS)",
                "name" => "js_lang"
            ));         
            
            */

        $yes_no = array(
            "Yes" => "yes",
            "No" => "no"
        );

        $this->add_custom_field( "title", "title_label_nav", "Navbar label (E.g. Inside Mosul)" )->type_textarea();
        $this->add_custom_field( "title", "title_label_report", "Special Report label (E.g. VOA Special Report)" )->type_textarea();
        $this->add_custom_field( "title", "title_tagline", "Tagline: e.g. Living in a city under seige" )->type_textarea();
        $this->add_custom_field( "title", "title_image", "Banner image url (via Pangea)" )->type_textarea();
        $this->add_custom_field( "title", "title_image_cutline", "Banner image cutline" )->type_textarea();

        //$this->add_custom_field( "title", "title_label_profile_grid", "Label: Profile grid" )->type_textarea();
        $this->add_custom_field( "title", "title_label_secondary_stories", "Label: Secondary stories" )->type_textarea();
        //$this->add_custom_field( "title", "title_label_footer", "Footer label (About the authors)" )->type_textarea();
        $this->add_custom_field( "title", "title_label_translations", "Label: Translations" )->type_textarea();
        $this->add_custom_field( "title", "title_label_voices", "Label: Voices" )->type_textarea();
        $this->add_custom_field( "title", "title_label_transcript", "Label: Transcript" )->type_textarea();


        $this->add_custom_field( "title", "title_label_facebook", "Label: Share on Facebook" )->type_textarea();
        $this->add_custom_field( "title", "title_label_twitter", "Label: Share on Twitter" )->type_textarea();


        $this->add_custom_field( "title", "meta_year", "Meta: Pub Year (2017)" )->type_textarea();
        $this->add_custom_field( "title", "meta_month", "Meta: Pub Month (05)" )->type_textarea();
        $this->add_custom_field( "title", "meta_day", "Meta: Pub Day (17)" )->type_textarea();
        $this->add_custom_field( "title", "meta_weekday", "Meta: Pub weekday (Wednesday)" )->type_textarea();
        $this->add_custom_field( "title", "meta_byline", "Meta: Pub byline (e.g.: Last Name, First Name of Byline1; Last Name, First Name of Byline2)" )->type_textarea();




        $this->add_custom_field( "title", "title_metrics_year", "Metrics: Year" )->type_textarea();
        $this->add_custom_field( "title", "title_metrics_month", "Metrics: Month" )->type_textarea();
        $this->add_custom_field( "title", "title_metrics_date", "Metrics: Date" )->type_textarea();
        $this->add_custom_field( "title", "title_metrics_day", "Metrics: Day" )->type_textarea();
        $this->add_custom_field( "title", "title_metrics_byline", "Metrics: Byline" )->type_textarea();
        $this->add_custom_field( "title", "title_metrics_slug", "Metrics: Slug" )->type_textarea();

        $this->add_custom_field( "story", "kicker", "Kicker label" )->type_textarea();
        $this->add_custom_field( "story", "byline", "Byline" )->type_textarea();
        $this->add_custom_field( "story", "image", "Image url (Pangea)" )->type_textarea();
        $this->add_custom_field( "story", "video", "YouTube ID (e.g. DlWcQ5sZMyE)" )->type_textarea();



        $this->options[] = 
            new timeline_renderer_option_select( array(
                "label" => "Language",
                "name" => "language",
                "default" => "Yes",
                "options" => array(
                    "en" => "English",
                    "arm" => "Armenian",
                    "az" => "Azerbaijani",
                    "my" => "Burmese",
                    "yue" => "Cantonese",
                    "ht" => "Creole",
                    "prs" => "Dari",
                    "deewa" => "Deewa",
                    "geo" => "Georgian",
                    "id" => "Indonesian",
                    "km" => "Khmer",
                    "ko" => "Korean",
                    "lo" => "Lao",
                    "zh" => "Mandarin",
                    "mac" => "Macedonian",
                    "ps" => "Pashto",
                    "ru" => "Russian",
                    "ser" => "Serbian",
                    "es" => "Spanish",
                    "tr" => "Turkish",
                    "urd" => "Urdu",
                    "vi" => "Vietnamese",
                    "ar" => "Arabic - RTL test"
                )
            ));

    }











    function before_publish( $preview = true ) {
        // wrapped for compatibility
        $this->new_before_publish();
    }
    
    // standard pre-publish will generate $this->files['json']
    function publish( $preview = true ) {
        global $VOA;

        // json is an object by default, true switch makes an array
        $slides = json_decode(file_get_contents( $this->files['json'] ), true);

        $VOA->assign( 'title', $this->title );
        $VOA->assign( 'options', $this->saved_options );
        $VOA->assign( 'slides', $slides );
        $VOA->assign( 'timeline_id', $this->timeline_id );
        
        $this->files['all'] = "{$this->folder}timeline_{$this->timeline_id}.js";
        
        // file_put_contents( $this->files['all'], $VOA->fetch( "unexpected-americans/template.tpl") );
        file_put_contents( $this->files['all'], $VOA->fetch( $this->saved_options->template ) );
        # file_put_contents( $this->files['all'], print_r($this->saved_options->template,true) );
        
        if( $preview === false ) {
            // $this->upload( $this->files['all'], basename($this->files['all']) );
        }
    }
    
        
    // normally this function would upload a file via ftp, but, not anymore
    function embed_code( $preview = true ) {
        return( file_get_contents($this->files['all']) );
    }
}
