# Simple boilerplate for creating VOA interactives #

This README would normally document whatever steps are necessary to get your application up and running.

### Getting started

If you don't have [`node-sass`](https://www.npmjs.com/package/node-sass) installed (or another way to compile SASS to CSS), you can run `sudo npm install -g node-sass`


### What is this repository for? ###

* Basic HTML markup (Navbar, menu, title card, footer, credits)
* Includes SASS files for standard CSS build
* Links to common JS libraries
* Sprites of common images (icons, logos and sharing)
* Code snippets for common tasks
* Three flavors for navbar: white, blue and dark
* Accessible tab menu in navbar
* Support for toggle for switching to translations
* Basic responsive grid
* Added supplemental CSS and SASS to help specify fonts for translations
* Added snippet for FB comments
* Added snippet for author bio in the footer

### To do list ###

* Standard logos

### Done (from the to-do list ###

* ~~set up SASS files~~
* ~~Add navbar markup, CSS and optional JS (Dark gray, white, transparent?)~~
* ~~Add dark/white options for navbar~~
* ~~Share buttons~~
* ~~analytics tracking~~
* ~~Translation~~

### Resources ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)