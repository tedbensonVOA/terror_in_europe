//Grab the spreadsheet KEY from the URL bar (NOT from the published window)
var public_spreadsheet_url = '1s20hMJfiFfYxV7DMQD7bR9TtiKm8UUnbC6kNuWXG2s0';
var map; //defined here for global access.
var myData;
var myDataLength;
var currentAttackNumber = 0;

var debugMode = true;
// Basic function to replace console.log() statements so they can all be disabled as needed;
function logger(logString){
	if (debugMode){
		console.log(logString);
	}
}








// ============================
// |  Basic tabletopJS setup  |
// ============================
function loadSpreadsheet() {
	if ( mode == "editing") {
		Tabletop.init( { key: public_spreadsheet_url,
			callback: showInfo,
			simpleSheet: true } )
		/*
		//multisheet version: 
		Tabletop.init( { key: public_spreadsheet_url,
		 	callback: showInfo,
		 	wanted: [ "religion", "parties" ] } )
		*/
	} else if ( mode == "production") {
		//buildPresidents(d3target);
		showInfo(bakedData);
	} else {
		console.log("You need to define the 'mode' ('editing' or 'production')");
	}
}



function showInfo(data) {
	logger("loaded spreadsheet data: ");

	// I'm storing the data we load as "myData" for future reference
	// When they click a thumbnail (or something) we'll look grab the relevant 'attack data' and present it
	myData = data;
	myDataLength = data.length


	// We're rendering the row of thumbnails.
	// I'm using a basic for-loop because the `$(data).each` functino was causing issues when i attached the onClick handler
	var thumbnails = "";
	for (var i = 0; i < myDataLength; i++ ){
		thumbnails += '<div class="attacks-nav" id="' + data[i].Key + '" data-number=' + i + ' data-toggle="popover" title="' + data[i].ThumbName + '" data-content="' + data[i].Date + '" title="' + data[i].ThumbName + '"><img src="' + data[i].ThumbPic + '" /></div>';
	}

	$("#links").html(thumbnails);

	// Add an onClick handler to the thumbnails
	// Set the currentAttackNumber and then call the function that updates the current attack profile
	$(".attacks-nav").click(function(){
		currentAttackNumber = Number( $(this).data("number") );
		showCurrent(currentAttackNumber);
	})

	// Function for updating the current attack card.
	// Because we're creating the map at the beginning, 
	// I don't want to erase it every time we click on a new thumbnail.
	// That's why i'm updating the individual IDs instead of just passing one big hunk-o-HTML 
	function showCurrent(currentNumber){

		var currentAttack = "";

		currentAttack += '<h3 id="attackDate" class="voa__kicker">' + myData[currentNumber].Date + '</h3>'
		currentAttack += '<h2 id="attackTitle" class="voa__attack__title">' + myData[currentNumber].Title + '</h2>'
		currentAttack += '<div id="attackDescription">' + myData[currentNumber].Description + '</div>';
		$("#attackText").html(currentAttack);

		$("#attackPhoto").attr("src", myData[currentNumber].Photolink);
		$("#attackMap").removeClass("lr ur ll ul");
		$("#attackMap").addClass(" " + myData[currentNumber].MapPos);
		$("#attackPhotoCaption").html(myData[currentNumber].Photocaption);

		map.setView([data[currentNumber].Lat, data[currentNumber].Lng], data[currentNumber].Zoom);
	}


	// This is where I'm initializing the map
	// By the way, you can find other map tiles here:
	// https://leaflet-extras.github.io/leaflet-providers/preview/
	// you can even mix and match so that the base layer can be painted in watercolor but the roads and labels are somehting else
	var mbToken = 'pk.eyJ1IjoiYndpbGxpYW1zb24iLCJhIjoiY2l0NjU5YWZhMDB0MjJ6cGd5bGU2dDd1cSJ9.4Bv8jg7AH5ksTrEvZyyjoQ';
	var tilesetUrl = 'https://a.tiles.mapbox.com/v4/mapbox.outdoors/{z}/{x}/{y}@2x.png?access_token='+mbToken;
	var tiles = L.tileLayer(tilesetUrl, {
		maxZoom: 18
	});

	var map = L.map("attackMap", {
		minZoom: 4,
		attributionControl: false,
		scrollWheelZoom: false,
		layers: [tiles]
	});


	// After loading the data and initializing the map, we load the default "attack profile"
	showCurrent(currentAttackNumber);


	// Adding basic previous/next buttons to make it easier to cycle through all of the attacks
	$("#nextButton").click(function(e){
		if ( (currentAttackNumber + 1) < myDataLength){
			currentAttackNumber++;
		} else {
			currentAttackNumber = 0;
		}
		showCurrent(currentAttackNumber);

		return false;
	})

	$("#previousButton").click(function(e){
		if ( (currentAttackNumber - 1) >= 0){
			currentAttackNumber--;
		} else {
			currentAttackNumber = myDataLength - 1;
		}
		showCurrent(currentAttackNumber);

		return false;
	})


}



$(document).ready(function(){
	logger("Ready");

	// ===================
	// |  Dropdown menu  |
	// ===================
	$(function() {
		$('#main-menu').smartmenus({
			subMenusSubOffsetX: 1,
			subMenusSubOffsetY: -8
		});
	});

	$( "#menuButton" ).click(function() {
		logger("clicked menu toggle")
	  $( ".main-menu-nav").toggle();
	});

	$( ".main-menu-nav a").not(".has-submenu").click(function() {
	  $( ".main-menu-nav").hide();
	});

	$( ".voa__section__full-width" ).click(function() {
	  $( ".main-menu-nav").hide();
	});




	/*
	// ================================================
	// |  Opens a pop-up with twitter sharing dialog  |
	// ================================================
	$('#shareTwitter').click(function(){
		var url = "http://projects.voanews.com";
		var text = "Replace this with your text";
		window.open('http://twitter.com/share?url='+encodeURIComponent(url)+'&text='+encodeURIComponent(text), '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
	})
	*/



	// =====================================
	// |  load spreadsheet via tabletopJS  |
	// =====================================
	loadSpreadsheet();


});	
