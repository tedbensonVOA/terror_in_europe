<?php

// ============================================================================
// makes sure that slugs are authoritative/canonical URLs, otherwise
// otherwise, redirect to the proper canonical for that ID
// ============================================================================
function enforce_slug($id, $slug) {
    $appropriate_slug = slug_for_slide($id);

    // page actually missing :/
    if( $appropriate_slug === false ) {

        // this should technically be a 404
        die( "Error: page not found" );
    }

    // page exists, but, someone typo'd the slug
    if( $appropriate_slug != $slug ) {
        $actual_url = url_for_slide_id($id);

        // 302 redirect to proper URL
        header("location:{$actual_url}");
        die;
    }
}

// ============================================================================
// strip long spaces and special characters, lowercase, replace spaces with -
//  input example: "   How this function works 13! "
// output example: "how-this-function-works-13"
// ============================================================================
function slugify($title) {

    $slug = strtolower($title);
    $slug = preg_replace("/[^a-zA-Z0-9\ ]+/", "", $slug);
    $slug = preg_replace("/[\s]+/", " ", $slug);
    $slug = str_replace(" ", "-", $slug);

    return( $slug );
}

// ============================================================================
// given a slide, it generates a URL for the slide - {$slide|makeurl}
// ============================================================================
function smarty_modifier_makeurl($slide) {

    if( !isset( $_GET['lang'] ) ) {
        $lang = "";
    } else {
        $lang = "/" . $_GET['lang'];
    }

    $url = sprintf(
        "%s%s/%s/%s-%s",
        HOME,
        $lang,
        ($slide["series"] == "profile" ? "profile" : "story"),
        $slide["id"],
        slugify($slide["title"])
    );

    // preview cache-busting
    if(
        isset( $_GET['nocache'] ) &&
        isset( $_GET['ts']) &&
        isset( $_GET['rand'] )
    ) {
        $url .= "?nocache=1&ts=" . $_GET['ts'] . "&rand=" . $_GET['rand'];
    }

    return( $url );
}

// ============================================================================
// search timeline global, return slide you want
// ============================================================================
function get_slug_by_id($id) {
    global $timeline;

    foreach( $timeline["slides"] as $slide ) {
        if( intval($slide["id"]) === intval($id) ) {
            return( $slide );
        }
    }

    // not found :/
    return( false );
}

// ============================================================================
// given a timeline ID, return proper URL for it
// ============================================================================
function url_for_slide_id($id) {

    $slide = get_slug_by_id($id);
    if( $slide === false) return( false );

    $url = smarty_modifier_makeurl($slide);

    return( $url );
}

// ============================================================================
// given a timeline ID, return proper slug for it
// ============================================================================
function slug_for_slide($id) {

    $slide = get_slug_by_id($id);
    if( $slide === false) return( false );

    return( slugify($slide["title"]) );
}
