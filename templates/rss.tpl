{function name="pangea_image"}{strip}
    {$a = "_"|explode:$url}
    {$url =$a[0]}
	{$url|regex_replace:"/\.jpg/i":""}_w{$width}_h{$height}.jpg
{/strip}{/function}{strip}

{$rss = simplexml_load_file($url)}


<!--

{foreach $rss->channel->item as $item}
{if $item@first}
<div class="voa__grid__one-third">
	<a href="{$item->link}">
		<img src="{pangea_image width=400 height=267 url=$item->enclosure.url}" />
		<h3>{$item->title}</h3>
	</a>
</div>
{/if}
{/foreach}
-->

	<ul class="voa__rss">
{foreach $rss->channel->item as $item}
{if $item@index < 4}
		<li>
			<a href="{$item->link}">{$item->title}</a>
		</li>
{/if}
{/foreach}



	</ul>
