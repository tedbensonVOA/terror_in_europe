<?php

//Update the class name (with underscores between words)
//Update the public $name and $folder 

class timeline_renderer_project_name_here extends timeline_renderer {
    public $name = 'Project name';
    public $folder = "renderers/project-name-here/";
    
    // 2016-06-19: new feature addition
    public $chronological = false;
    
    function declare_options() {
        
        // gah, superfix for superincludes bla
        $this->xfolder = "renderers/unexpected-americans/";

        $this->folder = realpath($this->folder);

        $this->options[] = 
            new timeline_renderer_option( array(
                "label" => "folder/template.tpl",
                "name" => "template"
            ));
            /*
        $this->options[] = 
            new timeline_renderer_option( array(
                "label" => "RSS URL",
                "name" => "rss_url"
            ));
            */
            /*
        $this->options[] = 
            new timeline_renderer_option( array(
                "label" => "Language (JS)",
                "name" => "js_lang"
            ));         
            
            */

        $yes_no = array(
            "Yes" => "yes",
            "No" => "no"
        );

        $this->add_custom_field( "title", "title_label_nav", "Navbar label (E.g. Inside Mosul)" )->type_textarea();
        $this->add_custom_field( "title", "title_label_report", "Special Report label (E.g. Special Report)" )->type_textarea();
        $this->add_custom_field( "title", "title_tagline", "Tagline: e.g. Living in a city under seige" )->type_textarea();
        $this->add_custom_field( "title", "title_description", "Description: tweet-length description for meta tags" )->type_textarea();

        $this->add_custom_field( "title", "title_image", "Banner image url (via Pangea)" )->type_textarea();
        $this->add_custom_field( "title", "title_image_cutline", "Banner image cutline" )->type_textarea();

        //$this->add_custom_field( "title", "title_label_profile_grid", "Label: Profile grid" )->type_textarea();
        $this->add_custom_field( "title", "title_label_secondary_stories", "Label: Secondary stories" )->type_textarea();
        $this->add_custom_field( "title", "title_label_RSS_headlines", "Label: Recent headlines (e.g. Recent VOA headlines)" )->type_textarea();

        $this->add_custom_field( "title", "title_label_facebook", "Label: Share on Facebook" )->type_textarea();
        $this->add_custom_field( "title", "title_label_twitter", "Label: Share on Twitter" )->type_textarea();


        $this->add_custom_field( "title", "title_metrics_year", "Metrics: Year" )->type_textarea();
        $this->add_custom_field( "title", "title_metrics_month", "Metrics: Month" )->type_textarea();
        $this->add_custom_field( "title", "title_metrics_date", "Metrics: Date" )->type_textarea();
        $this->add_custom_field( "title", "title_metrics_day", "Metrics: Day" )->type_textarea();
        $this->add_custom_field( "title", "title_metrics_byline", "Metrics: Byline" )->type_textarea();
        $this->add_custom_field( "title", "title_metrics_slug", "Metrics: Slug" )->type_textarea();

        $this->add_custom_field( "story", "kicker", "Kicker label" )->type_textarea();
        $this->add_custom_field( "story", "byline", "Byline" )->type_textarea();
        $this->add_custom_field( "story", "image", "Image url (Pangea)" )->type_textarea();
        $this->add_custom_field( "story", "video", "YouTube ID (e.g. DlWcQ5sZMyE)" )->type_textarea();



        $this->options[] = 
            new timeline_renderer_option_select( array(
                "label" => "Language",
                "name" => "language",
                "default" => "Yes",
                "options" => array(
                    "en" => "English",
                    "alb" => "Albanian",
                    "arm" => "Armenian",
                    "az" => "Azerbaijani",
                    "ban" => "Bangla",
                    "ba" => "Bosnian",
                    "my" => "Burmese",
                    "yue" => "Cantonese",
                    "ht" => "Creole",
                    "prs" => "Dari",
                    "deewa" => "Deewa",
                    "geo" => "Georgian",
                    "id" => "Indonesian",
                    "km" => "Khmer",
                    "ko" => "Korean",
                    "krds" => "Kurdish",
                    "lo" => "Lao",
                    "zh" => "Mandarin",
                    "mac" => "Macedonian",
                    "ps" => "Pashto",
                    "ru" => "Russian",
                    "ser" => "Serbian",
                    "es" => "Spanish",
                    "swa" => "Swahili",
                    "tr" => "Turkish",
                    "ukr" => "Ukrainian",
                    "urd" => "Urdu",
                    "uzb" => "Uzbek",
                    "vi" => "Vietnamese",
                    "ar" => "Arabic - RTL test"
                )
            ));

    }











    function before_publish( $preview = true ) {
        // wrapped for compatibility
        $this->new_before_publish();
    }
    
    // standard pre-publish will generate $this->files['json']
    function publish( $preview = true ) {
        global $VOA;

        // json is an object by default, true switch makes an array
        $slides = json_decode(file_get_contents( $this->files['json'] ), true);

        $VOA->assign( 'title', $this->title );
        $VOA->assign( 'options', $this->saved_options );
        $VOA->assign( 'slides', $slides );
        $VOA->assign( 'timeline_id', $this->timeline_id );
        
        $this->files['all'] = "{$this->folder}timeline_{$this->timeline_id}.js";
        
        // file_put_contents( $this->files['all'], $VOA->fetch( "unexpected-americans/template.tpl") );
        file_put_contents( $this->files['all'], $VOA->fetch( $this->saved_options->template ) );
        # file_put_contents( $this->files['all'], print_r($this->saved_options->template,true) );
        
        if( $preview === false ) {
            // $this->upload( $this->files['all'], basename($this->files['all']) );
        }
    }
    
        
    // normally this function would upload a file via ftp, but, not anymore
    function embed_code( $preview = true ) {
        return( file_get_contents($this->files['all']) );
    }
}
