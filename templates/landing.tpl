{extends file="template.tpl"}

{block name="content"}



		<section class="voa__section__full-width voa__section__title" style="background-image: url('{$slides[0].title_image}'); z-index: 9999;">
			<div class="voa__title-card__shadow"></div>

			<div class="" style="position: absolute; top: 30px; width: 100%; text-align: center;">
				<h3 class="voa__kicker" style="background-color:#000; font-size: 18px;">{$slides[0].title_label_report}</h3>
			</div>

			<div style="position: absolute; bottom: 20px; width: 100%;">
				<div class="voa__title-card" style="max-width: 1080px; margin: 0 auto; width: 100%;">
					<h1 class="voa__title" style="color: #FFF; text-shadow: 1px 1px 4px rgba(20,20,20,.5);  padding-left: 5%;">{$slides[0].title}</h1>
					<!--<h2 class="voa__title__tagline" style="color: rgba(240,240,240,.8); text-transform: uppercase; font-weight: normal; margin-top: 0;">{$slides[0].title_tagline}</h2>-->
				</div>
			</div>
		</section>


		<section class="voa__section__full-width">
			<div class="voa__grid__full">
				<div class="voa__grid">
					<div class="voa__project-title" style="margin: 0 0 20px 0;">
						<h3 id="projectIntro" class="voa__graphic__readin">{$slides[0].content|strip_tags}</h3>
					</div>
				</div>
			</div>
		</section>


{/block}
