{*<!--

    look for $options->js_lang field

-->*}{$language_info =[
    "en" => ["abbreviation" => "en", "territory" => "US", "name" => "english", "service" => "VOA English", "service_short" => "ENG", "property_id" => "600", "url" => "//voanews.com/", "direction" => "ltr", "translation" => "English"],


    "my" => ["abbreviation" => "my", "territory" => "XX", "name" => "burmese", "service" => "VOA Burmese", "service_short" => "BUR", "property_id" => "631", "url" => "//burmese.voanews.com/", "direction" => "ltr", "translation" => "Burmese"],

    "id" => ["abbreviation" => "id", "territory" => "ID", "name" => "indonesian", "service" => "VOA Indonesian", "service_short" => "IND", "property_id" => "634", "url" => "//www.voaindonesia.com/", "direction" => "ltr", "translation" => "Indonesian"],

    "km" => ["abbreviation" => "km", "territory" => "XX", "name" => "khmer", "service" => "VOA Khmer", "service_short" => "KHM", "property_id" => "604", "url" => "//khmer.voanews.com/", "direction" => "ltr", "translation" => "ខ្មែរ"],

    "ko" => ["abbreviation" => "ko", "territory" => "KO", "name" => "korean", "service" => "VOA Korean", "service_short" => "KOR", "property_id" => "635", "url" => "//www.voakorea.com/", "direction" => "ltr", "translation" => "Korean"],

    "lo" => ["abbreviation" => "en", "territory" => "LAO", "name" => "lao", "service" => "VOA Lao", "service_short" => "LAO", "property_id" => "636", "url" => "//lao.voanews.com/", "direction" => "ltr", "translation" => "ລາວ"],

    "yue" => ["abbreviation" => "yue", "territory" => "KO", "name" => "cantonese", "service" => "VOA Cantonese", "service_short" => "CAN", "property_id" => "632", "url" => "//www.voacantonese.com/", "direction" => "ltr", "translation" => "粤语"],

    "zh" => ["abbreviation" => "zh-cn", "territory" => "CH", "name" => "mandarin", "service" => "VOA Chinese", "service_short" => "MAN", "property_id" => "610", "url" => "//www.voachinese.com/", "direction" => "ltr", "translation" => "國語"],

    "th" => ["abbreviation" => "th", "territory" => "XX", "name" => "thai", "service" => "VOA Thai", "service_short" => "THAI", "property_id" => "637", "url" => "//www.voathai.com/", "direction" => "ltr", "translation" => "ไทย"],

    "vi" => ["abbreviation" => "vi", "territory" => "VN", "name" => "vietnamese", "service" => "VOA Vietnamese", "service_short" => "VIE", "property_id" => "617", "url" => "//www.voatiengviet.com/", "direction" => "ltr", "translation" => "Tiếng Việt"],



    "az" => ["abbreviation" => "az", "territory" => "XX", "name" => "azerbaijani", "service" => "VOA Azerbaijani", "service_short" => "AZ", "property_id" => "621", "url" => "//www.amerikaninsesi.org/", "direction" => "ltr", "translation" => "Azerbaijani"],    

    "ban" => ["abbreviation" => "ban", "territory" => "XX", "name" => "bangla", "service" => "VOA Bangla", "service_short" => "BAN", "property_id" => "625", "url" => "//www.voabangla.com/", "direction" => "ltr", "translation" => "Bangla"],    

    "deewa" => ["abbreviation" => "deewa", "territory" => "XX", "name" => "deewa", "service" => "Radio Deewa", "service_short" => "DEEWA", "property_id" => "640", "url" => "//www.voadeewaradio.com/", "direction" => "rtl", "translation" => "Deewa"],

    "krds" => ["abbreviation" => "krds", "territory" => "XX", "name" => "kurdish", "service" => "Radio Kurdish", "service_short" => "KRDS", "property_id" => "623", "url" => "//www.dengiamerika.com/", "direction" => "rtl", "translation" => "Kurdish"],

    "prs" => ["abbreviation" => "prs", "territory" => "XX", "name" => "dari", "service" => "VOA Afghanistan", "service_short" => "AFDA", "property_id" => "601", "url" => "//www.darivoa.com/", "direction" => "rtl", "translation" => "Dari"],

    "ps" => ["abbreviation" => "ps", "territory" => "XX", "name" => "pashto", "service" => "VOA Afghanistan", "service_short" => "AFPA", "property_id" => "602", "url" => "//www.pashtovoa.com/", "direction" => "rtl", "translation" => "Pashto"],

    "per" => ["abbreviation" => "fa", "territory" => "XX", "name" => "persian", "service" => "VOA Persian", "service_short" => "PER", "property_id" => "611", "url" => "//ir.voanews.com/", "direction" => "rtl", "translation" => "Persian"],

    "tr" => ["abbreviation" => "tr", "territory" => "XX", "name" => "turkish", "service" => "VOA Turkish", "service_short" => "TURK", "property_id" => "615", "url" => "//www.amerikaninsesi.com/", "direction" => "ltr", "translation" => "Turkish"],

    "urd" => ["abbreviation" => "urd", "territory" => "XX", "name" => "urdu", "service" => "VOA Urdu", "service_short" => "URD", "property_id" => "616", "url" => "//www.urduvoa.com/", "direction" => "rtl", "translation" => "Urdu"],

    "uzb" => ["abbreviation" => "urd", "territory" => "XX", "name" => "Uzbek", "service" => "VOA Uzbek", "service_short" => "UZB", "property_id" => "618", "url" => "//www.amerikaovozi.com/", "direction" => "ltr", "translation" => "Uzbek"],




    "ht" => ["abbreviation" => "ht", "territory" => "HTI", "name" => "creole", "service" => "VOA Creole", "service_short" => "CREO", "property_id" => "633", "url" => "//www.voanouvel.com/", "direction" => "ltr", "translation" => ""],

    "es" => ["abbreviation" => "es", "territory" => "MX", "name" => "spanish", "service" => "VOA Spanish", "service_short" => "SPA", "property_id" => "630", "url" => "//www.voanoticias.com/", "direction" => "ltr", "translation" => ""],




    "alb" => ["abbreviation" => "alb", "territory" => "XX", "name" => "albanian", "service" => "VOA Albanian", "service_short" => "ALB", "property_id" => "624", "url" => "//www.zeriamerikes.com/", "direction" => "ltr", "translation" => "Shqip"],    

    "arm" => ["abbreviation" => "arm", "territory" => "XX", "name" => "armenian", "service" => "VOA Armenian", "service_short" => "ARM", "property_id" => "638", "url" => "//www.amerikayidzayn.com/", "direction" => "ltr", "translation" => ""],

    "ba" => ["abbreviation" => "ba", "territory" => "XX", "name" => "bosnian", "service" => "VOA Bosnian", "service_short" => "BOS", "property_id" => "626", "url" => "//ba.voanews.com/", "direction" => "ltr", "translation" => "Bosanski"],    

    "geo" => ["abbreviation" => "geo", "territory" => "XX", "name" => "georgian", "service" => "VOA Georgian", "service_short" => "GEO", "property_id" => "627", "url" => "//www.amerikiskhma.com/", "direction" => "ltr", "translation" => ""],

    "mac" => ["abbreviation" => "mac", "territory" => "XX", "name" => "macedonian", "service" => "VOA Macedonian", "service_short" => "MAC", "property_id" => "629", "url" => "//mk.voanews.com/", "direction" => "ltr", "translation" => ""],

    "ru" => ["abbreviation" => "ru", "territory" => "XX", "name" => "russian", "service" => "VOA Russian", "service_short" => "RU", "property_id" => "612", "url" => "//www.golos-ameriki.ru/", "direction" => "ltr", "translation" => "Русский"],

    "ser" => ["abbreviation" => "ser", "territory" => "XX", "name" => "serbian", "service" => "VOA Serbian", "service_short" => "SER", "property_id" => "641", "url" => "//www.glasamerike.net/", "direction" => "ltr", "translation" => "Srpski"],

    "ukr" => ["abbreviation" => "ukr", "territory" => "XX", "name" => "ukrainian", "service" => "VOA Ukrainian", "service_short" => "UKR", "property_id" => "642", "url" => "//ukrainian.voanews.com/", "direction" => "ltr", "translation" => "Українська"],




    "swa" => ["abbreviation" => "afr", "territory" => "XX", "name" => "Swahili", "service" => "VOA Swahili", "service_short" => "SWA", "property_id" => "614", "url" => "//www.voaswahili.com/", "direction" => "ltr", "translation" => "Kiswahili"],



    "ar" => ["abbreviation" => "ar", "territory" => "XX", "name" => "arabic", "service" => "VOA Arabic - rtl test", "service_short" => "AR", "property_id" => "XXX", "url" => "//www.voanews.com/", "direction" => "rtl", "translation" => ""]
]}{if isset($want)}{if isset($want_language)}{$language_info[$want_language][$want]}{else}{$language_info[$options->language][$want]}{/if}{else}

<script type="text/javascript">
var utag_data = {
    entity:"VOA",
    language:"{$language_info[$options->language].name}",
    language_service:"{$language_info[$options->language].service}",
    short_language_service:"{$language_info[$options->language].service_short}",
    property_id:"{$language_info[$options->language].property_id}",
    platform:"Responsive",
    platform_short:"R",
    runs_js:"Yes",
    section:"Special Projects",
    english_section:"special-projects",
    page_title:"{$slides[0].title}",
    page_type:"interactive",
    page_name:"{$slides[0].title}",
    short_headline:"{$slides[0].title}",
    long_headline:"{$slides[0].title}",
    headline:"{$slides[0].title}",
    content_type:"interactive",
    pub_year:"{$slides[0].title_metrics_year}",
    pub_month:"{$slides[0].title_metrics_month}",
    pub_day:"{$slides[0].title_metrics_date}",
    pub_weekday:"{$slides[0].title_metrics_day}",
    byline:"{$slides[0].title_metrics_byline}",
    slug:"{$slides[0].title_metrics_slug}"
}
</script>

{/if}
