{strip}

{foreach from=$slides item=slide}
{if $slide.id == $id}{$video = $slide}{/if}
{/foreach}


{$fb_app_id = '490219017808002'}

{* $canonical_url = 'https://projects.voanews.com/mosul/' *}
{assign var="canonical_url" value="{if $id == -1}https://projects.voanews.com/poaching/{else}https:{$video|makeurl}{/if}"}

{$voa_homepage_url = 'https://www.voanews.com/'}
{assign var="twitter_share_text" value="{if $id == -1}{$slides[0].title_label_report}: {$slides[0].title|strip_tags} {else}{$slides[0].title_label_nav}: {$video.title}{/if}"}
{$twitter_username = '@VOANews'}
{$twitter_related = ''}

{assign var="fb_action_properties" value=['object'=>$canonical_url]}

{/strip}<!doctype html>

<html lang="{$options.language}" dir="ltr{* include file='templates/metrics.tpl' want='direction' *}">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />


{if $id == -1}
	<title>{$slides[0].title|strip_tags}: {$slides[0].title_tagline}</title>


    <link rel="canonical" href="https:{$canonical}" />
    <link type="image/x-icon" rel="icon" href="https://www.voanews.com/img/voa/favicon.ico" />
    <link rel="image_src" href="{$slides[0].title_image|regex_replace:'/(?<=_)(.*)(?=.jpg)/':"_w1800_s"}" />


	<!-- for Google -->
	<meta name="description" content="{$slides[0].title_description}" />
	<meta name="keywords" content="Mosul, Iraq, ISIS" />
	<meta name="author" content="{$slides[0].title_metrics_byline}" />

	<!-- for Facebook -->
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="{$slides[0].title|strip_tags}: {$slides[0].title_tagline}" />
	<meta property="og:description" content="{$slides[0].title_description}" />
	<meta property="og:image" content="{$slides[0].title_image|regex_replace:'/(?<=_)(.*)(?=.jpg)/':"_w1800_s"}" />
	<meta property="og:url" content="https:{$canonical}" />

	<!-- for Twitter -->
	<meta property="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@voanews" />
	<meta name="twitter:creator" content="@voanews" />
	<meta property="twitter:title" content="{$slides[0].title|strip_tags}: {$slides[0].title_tagline}" />
	<meta property="twitter:description" content="{$slides[0].title_description}" />
	<meta property="twitter:image" content="{$slides[0].title_image|regex_replace:'/(?<=_)(.*)(?=.jpg)/':"_w1800_s"}" />
    <meta name="twitter:url" content="https:{$canonical}" />


{else}
	<title>{$slides[0].title_label_nav}: {$video.title}</title>


    <link rel="canonical" href="https:{$video|makeurl}" />
    <link type="image/x-icon" rel="icon" href="//www.voanews.com/img/voa/favicon.ico" />
    <link rel="image_src" href="{$video.image}" />


	<!-- for Google -->
	<meta name="description" content="{if $video.description != ""}{$video.description}{else}{$video.summary}{/if}" />
	<meta name="keywords" content="Mosul, Iraq, ISIS, Islamic State" />
	<meta name="author" content="{$video.byline}" />

	<!-- for Facebook -->
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="{$slides[0].title_label_nav}: {$video.title}" />
	<meta property="og:description" content="{if $video.description != ""}{$video.description}{else}{$video.summary}{/if}" />
	<meta property="og:image" content="{$video.image}" />
	<meta property="og:url" content="https:{$video|makeurl}" />

	<!-- for Twitter -->
	<meta property="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@voanews" />
	<meta name="twitter:creator" content="@voanews" />
	<meta property="twitter:title" content="{$slides[0].title_label_nav}: {$video.title}" />
	<meta property="twitter:description" content="{if $video.description != ""}{$video.description}{else}{$video.summary}{/if}" />
	<meta property="twitter:image" content="{$video.image}" />
    <meta name="twitter:url" content="https:{$video|makeurl}" />

{/if}



    <meta name="DISPLAYDATE" content="June {$slides[0].title_metrics_date}, 2017" />
    <meta itemprop="datePublished" content="2017-{$slides[0].title_metrics_month}-{$slides[0].title_metrics_date}" />


	<link href="//fonts.googleapis.com/css?family=Oswald" rel="stylesheet" />


	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script type="text/javascript" src="//www.voanews.com/MediaAssets2/projects/voa_graphics/resources/jquery.smartmenus.min.js"></script>

	<script type="text/javascript" src="//hammerjs.github.io/dist/hammer.js"></script>


	<script type="text/javascript" src="//www.voanews.com/MediaAssets2/projects/voa_graphics/resources/leaflet/js/leaflet.js"></script>
	<link rel="stylesheet" type="text/css" href="//www.voanews.com/MediaAssets2/projects/voa_graphics/resources/leaflet/css/leaflet.css" />


	<script>
	// homepage
	var project_home = {$home|json_encode};

	// canonical
	var project_url = {$canonical|json_encode};

	</script>


	<script src="{$home}/js/scripts.js?ts={$smarty.now}"></script>

	<link rel="stylesheet" type="text/css" href="{$home}/css/all.css?ts={$smarty.now}" />
</head>

<body class="{$options.language}">

{include file="metrics.tpl"}

	<div class="voa__project english{* include file='metrics.tpl' want='name' *}">

		<div class="voa__navbar">
			<div class="voa__grid__full">
				<div class="voa__grid">
					<a href="https://www.voanews.com/{* include file='metrics.tpl' want='url' *}" class="voa__navbar__link" title="Visit the VOA News homepage"><div class="voa__navbar__logo"></div></a><a href="{$home}/{if $options.language != "en"}{$options.language}/{/if}" class="voa__navbar__title">{$slides[0].title_label_nav}</a>

					<div id="menuButton" class="voa__navbar__toggle"></div>

					<nav id="main-nav" role="navigation" class="main-menu-nav">
						<ul id="main-menu" class="sm sm-clean" data-smartmenus-id="14762810590939192">

							<li><a id="shareTwitter" title="Share on Twitter" class="social-share-link twitter" href="https://twitter.com/intent/tweet?text={$twitter_share_text|escape:'url'}&amp;url={$canonical_url|escape:'url'}&amp;via={$twitter_username|replace:'@':''}&amp;related={$twitter_related|escape:'url'}"><div class="social-icon"></div><span class="social-share-text">{$slides[0].title_label_twitter}</span></a></li>
							<li><a id="shareFacebook" title="Share on Facebook" class="social-share-link facebook" href="https://www.facebook.com/dialog/share_open_graph?app_id={$fb_app_id}&amp;display=popup&amp;action_type=og.likes&amp;action_properties={$fb_action_properties|json_encode|escape:'url'}&amp;href={$canonical_url|escape:'url'}&amp;redirect_uri={$canonical_url|escape:'url'}"><div class="social-icon"></div><span class="social-share-text">{$slides[0].title_label_facebook}</span></a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>

{block name="content"}{/block}

		<section class="voa__section__full-width voa__footer dark " style="margin-top: 0;">
			<div class="voa__grid__full">
				<div class="voa__grid">

					<div class="voa__grid__one-third voa__rule wide-right">
						<h3 style="margin-bottom: 20px;">{$slides[0].title_label_recent_headlines}</h3>

						{include 
							note="feed-relay makes sure an RSS feed doesn't get hit more than once per 5 minutes"
							file="templates/rss.tpl" 
							url="http://projects.voanews.com/feed-relay/?url=http://www.voanews.com/api/zr\$opeuvim"
						}

					</div><!--


{assign var="counter" value=0}
{foreach from=$slides item=slide }
	{if $slide.series == "footer"}
					--><div class="voa__grid__one-third">
						<h3>{$slide.title}</h3>
						{$slide.content}
					</div><!--
	{/if}
{/foreach}
				-->
				</div>
			</div>
		</section><!-- .voa__footer -->


    </div><!-- .voa__project -->


</body>
</html>
