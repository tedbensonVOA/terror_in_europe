<?php
require( "vendor/autoload.php" );
require( "smarty_xpath.php" );

$smarty = new Smarty();
$smarty->setTemplateDir("./_templates/");


// THIS SETUP IS FOR SINGLE PAGE PROJECTS.
// FOR MULTIPAGE PROJECTS, USE home-multipage.php
//
// THE SINGLE PAGE SETUP USES TEMPLATES IN THE /_templates/ folder

// data source 1: timeline editor


// ============================================================================
// DATA: download slides.json from
// http://tools.voanews.com/utilities/timeline-editor/preview/?timeline=58182&api-json
// (Also possible to download live date by using that URL as API)
// ============================================================================
$data = json_decode(file_get_contents("_cache/slides.json"), true);
$data["options"] = (object)$data["options"];
foreach( $data as $key => $value ) {
    $smarty->assign( $key, $value );
}

// data source 2: google spreadsheet
/*
$entries = json_decode(file_get_contents("data/spreadsheet.json"), true);
$smarty->assign( "entries", $entries );
*/

$smarty->display( '00-template.tpl' );
