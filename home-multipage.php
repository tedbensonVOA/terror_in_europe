<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require( "config.php" );
require( "functions.php" );
require( "vendor/autoload.php" );


// THIS SETUP IS FOR MULTIPAGE PAGE PROJECTS.
// FOR SINGLE PROJECTS, USE home.php

// TO MAKE THIS WORK, BE SURE TO SET UP config.php, .htaccess
// MAKE SURE TO POINT TO THE CORRECT TIMELINE EDITOR API (this file, .htaccess )
// YOU MAY ALSO NEED TO EDIT THIS FILE AND FUNCTIONS.PHP TO MAKE SURE THE PROPER TEMPLATES ARE CALLED
// THE MULTIPAGE SETUP USES TEMPLATES IN THE /templates/ folder



$smarty = new Smarty();
$smarty->assign( "development_mode", DEVELOPMENT_MODE );
$smarty->assign( "home", HOME );

// ============================================================================
// DATA: download slides.json from
// tools.voanews.com/utilities/timeline-editor/preview/?timeline=58182&api-json
// ============================================================================
//$d = "cache/slides.json";
if( !isset( $_GET['timeline'] ) ) {
    $timeline = 59150; // en
} else {
    $timeline = intval($_GET['timeline']);
}
$d = "http://tools.voanews.com/utilities/timeline-editor/preview/?timeline={$timeline}&api-json";

$timeline = json_decode(file_get_contents($d), true) or die("Missing {$d}");
$smarty->assign( "slides", $timeline["slides"] );
$smarty->assign( "options", $timeline["options"] );
// ============================================================================
// passed-through via mod-rewrite: id, video, story querystring parameters
// ============================================================================

// no lang = english
$lang = "";
if( isset( $_GET['lang']) ) {
    $lang = "/{$_GET['lang']}";
}

if( isset( $_GET['id'] ) && isset($_GET['slug']) ) {
    $id = intval($_GET['id']);
    $slug = $_GET['slug'];

    // make sure this is the right slug
    // otherwise redirect to it (302)
    enforce_slug($id, $slug, $lang);

    // everything below this point should be legit
    $smarty->assign( "id", $id );

    // canonical, template
    if( isset( $_GET['profile']) ) {
        $template = "templates/profile.tpl";
        $canonical = HOME . $lang . "/profile/{$id}-{$slug}";
    } elseif( isset( $_GET['story']) ) {
        $template = "templates/story.tpl";
        $canonical = HOME . $lang . "/story/{$id}-{$slug}";
    } 
} else {
   // $template = "_timeline-editor/04-mosul.tpl";
    $template = "templates/landing.tpl";
    $canonical = HOME . $lang . "/";

    // variable is used by data-map routine
    $smarty->assign( "id", -1 );
}

// ============================================================================
// show all the
// ============================================================================
$smarty->assign( "canonical", $canonical );
$smarty->display( $template );
