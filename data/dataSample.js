var sampleData = {
"Finsbury20170619": {

	"title": "Finsbury Park terror attack" ,

	"date": "June 19 2017",

	"description": "A van driver mowed down Muslim worshippers on Seven Sisters Road near Finsbury Park mosque in the very early hours of June 19. 

One man was left dead, while eight injured victims were taken to hospital and two others were treated at the scene.

The suspect Darren Osborne has been charged with terror-related murder and attempted murder." ,
	
	"photolink": "https://gdb.voanews.com/907AF4CB-5C76-4728-8D1D-29E100CB5592.jpg",
	
	"photocaption": "A message on a wall is seen near to where a van was driven at Muslims in Finsbury Park, North London, Britain, June 19, 2017. REUTERS/Marko Djurica - RTS17SBD"
	},

"LondonBridge20170603": {

	"title": "London Bridge terror attack", 

	"date": "June 3 2017",

	"description": "The London terror attack killed eight people and injured many others on London Bridge and in nearby Borough Market on Saturday June 3. 

	Three knifemen were shot dead by police after mowing down pedestrians the bridge and going on a killing spree at pubs and resturants at 10pm." ,
	
	"photolink": "https://gdb.voanews.com/FB8E9E98-D5F7-4519-868C-C1A84C3E5185.jpg",
	
	"photocaption": "A police officer clears people away from the area near London Bridge after an incident in central London, late Saturday, June 3, 2017.  British police said they were dealing with "incidents" on London Bridge and nearby Borough Market in the heart of the British capital Saturday, as witnesses reported a vehicle veering off the road and hitting several pedestrians. (AP Photo/ Matt Dunham)"
	},

"Manchester20170522": {

	"title": "Manchester terror attack",

	"date": "May 22 2017",

	"description": "The Manchester terror attack killed at least 22 people and injured 59 others at an Ariana Grande concert at Manchester Arena on Monday May 22. 

	A lone suicide bomber detonated explosives among teenage fans leaving the concert at 10.33pm.",
	
	"photolink": "https://gdb.voanews.com/2C991092-ECFF-4D93-BD3D-9BB3F0DEF5C6.jpg",
	
	"photocaption": "Concert goers react after fleeing the Manchester Arena in northern England where U.S. singer Ariana Grande had been performing in Manchester, Britain, May 22, 2017. REUTERS/Jon Super - RTX373FY"
	},

"Paris20170420": {

	"title": "Paris shooting",

	"date": "April 20 2017",

	"description": "A policeman was killed on the Champs Elysees in Paris in what is being treated as a terror-related attack.

	ISIS have claimed responsibility for the killing, which comes just days before the French presidential election.

	The gunman has been named in the media as Karim Cheurfi, a 39-year-old man who allegedly served 15 years in prison for three attempted murders.

	The attacker was shot dead at the scene.",
	
	"photolink": "https://gdb.voanews.com/5FB388EC-9B3D-4A79-ADF7-0883E5F607A0.jpg",
	
	"photocaption": "A bullet hole is pictured on a shopwindow of the Champs Elysees boulevard in Paris, Friday, April 21, 2017. France began picking itself up Friday from another deadly shooting claimed by the Islamic State group, with President Francois Hollande convening the government's security council and his would-be successors in the presidential election campaign treading carefully before voting this weekend. (AP Photo/Christophe Ena)"
	},

"Stockholm20170407": {

	"title": "Stockholm attack",

	"date": "April 7 2017",

	"description": "Four people were killed and at least fifteen were injured when a man drove a truck down a busy shopping street.

	Rakhmat Akilov, a failed asylum seeker from Uzbekistan, has been arrested and charged in connection with the attack.

	The 39-year-old has allegedly admitted being a member of ISIS and told police investigators that he had 'achieved what he set out to do'.",
	
	"photolink": "https://gdb.voanews.com/8C4E082C-FD27-467B-B44B-A88B5572FCCF.jpg",
	
	"photocaption": "People were killed when a truck crashed into department store Ahlens on Drottninggatan, in central Stockholm, Sweden April 7, 2017. TT News Agency/Fredrik Sandberg/via REUTERS"
	},

"StPetersburg20170403": {

	"title": "St Petersburg attack",

	"date": "April 3 2017",

	"description": "A suicide bombing on the metro killed 14 people and injured dozens more.",
	
	"photolink": "",
	
	"photocaption": ""
	},

"Westminster20170322": {

	"title": "Westminster attack",

	"date": "March 22 2017",

	"description": "London attacker Khalid Masood mowed down pedestrians on Westminster Bridge, killing two men and two women and injuring many others. 

	The knifeman crashed his car into the railings outside Parliament, got out and ran into New Palace Yard where he stabbed a brave police officer to death. 

	Masood was shot dead by armed police.",
	
	"photolink": "https://gdb.voanews.com/0CC93C00-93F9-44E0-B2C8-C1CF52961DFA.jpg",
	
	"photocaption": "A woman lies injured after a shooting incident on Westminster Bridge in London, March 22, 2017.  REUTERS/Toby Melville   TPX IMAGES OF THE DAY - RTX326BU"
	},

"Louvre20170203": {

	"title": "Louvre knife attack",

	"date": "February 3 2017",

	"description": "A knifeman was shot while trying to attack a group of soldiers guarding the Louvre in Paris on February 3.

	The attacker reportedly cried ‘Allahu Akbar’ and drew a machete on the soldiers after being told he could not enter the Louvre Carrousel shopping centre with two backpacks. 

	Prime Minister Bernard Cazeneuve said the attack was 'clearly of terrorist nature' but said that no explosives were found in the bags. 

	Berlin Christmas market attack: December 19 2016

	Attacker Anis Amri drove a lorry into a packed Christmas market, killing 12 people and injuring more than 60, in Berlin.",
	
	"photolink": "https://gdb.voanews.com/60C35833-5317-4206-A6F6-869E7E51E99E.jpg",
	
	"photocaption": "Police officers cordon off the area outside the Louvre museum near where a soldier opened fire after he was attacked in Paris, Feb. 3, 2017. Police say the soldier opened fire outside the Louvre Museum after he was attacked by someone, and the area is being evacuated. (AP Photo/Christophe Ena)"
	}, 

"Normandy20160726": {

	"title": "Normandy church attack", 

	"date": "Tuesday July 26 2016",

	"description": "Armed men slit the throat of a priest and took several others hostage after storming a church during mass in Normandy on Tuesday July 26. 

	Two armed men stormed a church in Saint-Etienne-du-Rouvray, a suburb of Rouen in northern France.  

	The attackers slit the throat of elderly priest Father Jacques Hamel and took four other people hostage. One of hostages is fighting for his life. 

	Police have now shot dead the attackers. Islamic State (ISIS) has claimed that the men were soldiers of their sick cause.",
	
	"photolink": "https://gdb.voanews.com/A4BFFF74-AB74-412D-B005-A3AB5AB737BA.jpg",
	
	"photocaption": "French soldiers stand guard near the scene of an attack in Saint-Etienne-du-Rouvray, Normandy, France, Tuesday, July 26, 2016. Two attackers invaded a church Tuesday during morning Mass near the Normandy city of Rouen, killing an 84-year-old priest by slitting his throat and taking hostages before being shot and killed by police, French officials said. (AP Photo/Francois Mori)"
	},

"Germany20160718": {

	"title": "Attacks in Germany",
	
	"date": "July 18-26 2016",

	"description": "A doctor died after being shot in a Berlin hospital on Tuesday July 26 during the fifth horror attack in Germany in just over a week. 

	The string of violent attacks started when an axeman hacked passengers on a train in Wurzburg on Monday July 18. 

	A young Iranian-German guman went on a deadly rampage in Munich on Friday July 22 after being inspired by far-right killer Anders Breivik. 

	In two separate attacks on Sunday July 24, a man blew himself up in Ansbach and a man killed a pregnant woman duirng a machete attack in Reutlingen.",
	
	"photolink": "https://gdb.voanews.com/1ADE0054-179F-4A08-8082-315CBAB8874A.jpg",
	
	"photocaption": "German Chancellor Angela Merkel addresses the media during a statement in Berlin, Germany, Saturday, July 23, 2016 on the Munich attack."
	},
	
"Nice20160714": {

	"title": "Nice terror attack",

	"date": "July 14 2016", 

	"description": "A terrorist in a lorry mowed down revellers who had just finished watching a firework display to mark Bastille Day in France. 

	The horrific rampage killed 84 people and injured hundreds of others on the promenade in the seaside town of Nice. 

	The attacker Mohamed Lahouaiej Bouhlel, a 41-year-old Tunisian-born French citizen, was shot dead by security forces.",
	
	"photolink": "https://gdb.voanews.com/C6D12FBB-CD91-47C3-B177-AC9D8DFF1C21.jpg",
	
	"photocaption": "Police officers, firefighters and rescue workers are seen at the site of an attack on July 15, 2016, after a truck drove into a crowd watching a fireworks display in the French Riviera town of Nice."
	},

"Leeds20160616": {

	"title": "British MP murdered", 

	"date": "June 16, 2016",

	"description": "Jo Cox, MP for Batley and Spen, was murdered by far-right terrorist Thomas Mair.",
	
	"photolink": "",
	
	"photocaption": ""
	},

"Brussels20160322": {

	"title": "Brussels bombings", 

	"date": "March 22 2016",

	"description": "The Brussels bombings killed 32 people and wounded more than 300 other victims in a day of terror. 

	There were two suicide bombings at Brussels Airport and another bombing at a Metro station in the Belgium capital.",
	
	"photolink": "https://gdb.voanews.com/6473A852-BBEB-4BCA-8BE8-0517FFC97B4D.jpg",
	
	"photocaption": "Police and other emergency workers stand in front of the damaged Zaventem Airport terminal in Brussels on March 23, 2016. "
	},

"Marseille20160111": {

	"title": "Marseille attack", 

	"date": "January 11, 2016", 

	"description": "A teeanger attacked a Jewish teacher with a machete, claiming that he did so in the name of Isis.",
	
	"photolink": "",
	
	"photocaption": ""
	},

"Paris20151113": {

	"title": "Paris attacks", 

	"date": "November 13 2015", 

	"description": "A series of terrifying attacks in Paris killed 130 victims and injured hundreds of others. It was the most deadly assault on French soil since World War II. 

	A suicide bombing at the Stade de France stadium were followed by more explosions and shootings at popular bars and restaurants in Paris. 

	Three gunmen also opened fire at Bataclan concert hall and killed spectators who were watching the Eagles of Death Metal perform.",
	
	"photolink": "https://gdb.voanews.com/ECDAE047-A026-4098-AE06-B6B7B06CBADB.jpg",
	
	"photocaption": "Fans comfort each other after descending onto the playing field in Stade de France stadium at the end of the friendly soccer match between France and Germany in Saint Denis, outside Paris, Nov. 13, 2015, the night of the terror attacks."
	},


"Copenhagen20150215": {

	"title": "Copenhagen attack",
	
	"date": "February 15, 2015",
	
	"description": "A Danish national inspired by Isis killed two people and wounded five police officers."
	
	"photolink": "",
	"photocaption": ""
	},

"CharlieHebdo20150107": {

	"title": "Charlie Hebdo attack", 

	"date": "January 7 2015",

	"description": "Two masked gunmen carried out a bloody terror attack on the French satirical weekly newspaper Charlie Hebdo in Paris. 

	Brothers Saïd and Chérif Kouachi killed 12 people during the lunchtime massacre at the Charlie Hebdo offices in the French capital.

	A policewoman was killed a day later. On January 9, another terrorist killed four hostages at a Jewish supermarket.",
	
	"photolink": "https://gdb.voanews.com/6CE59078-0E71-49A7-8E69-E9582FC232E2.jpg",
	
	"photocaption": "Supporters of French magazine Charlie Hebdo, carrying placards reading "I am Charlie", stage a silent protest outside the Foreign Correspondent Club in Hong Kong January 8, 2015."
	
	}
	};